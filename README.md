# Astronaut Analysis

This analysis is based on publicly available astronauts data from [Wikidata](https://www.wikidata.org/wiki/Wikidata:Main_Page).
In this context, we investigated aspects such as time humans spent in space as well as the age distribution of the astronauts.

<img src="results/humans_in_space.png" alt="Total Time Human in Space" title="Total Time Human in Space" width="250" height="200" />
<img src="results/female_humans_in_space.png" alt="Total Time Females in Space" title="Total Time Females in Space" width="250" height="200" />
<img src="results/male_humans_in_space.png" alt="Total Time Males in Space" title="Total Time Males in Space" width="250" height="200" />
<img src="results/boxplot.png" alt="Age Distribution Box Plot" title="Age Distribution Box Plot" width="250" height="200" />
<img src="results/combined_histogram.png" alt="Age Distribution Histogram" title="Age Distribution Histogram" width="250" height="200" />

The repository is organized as follows:

- [data](data): Contains the astronauts data set retrieved from Wikidata
- [docs](docs): Contains the documentation
- [code](code): Contains the astronaut analysis script
- [results](results): Contains the resulting analysis plots
