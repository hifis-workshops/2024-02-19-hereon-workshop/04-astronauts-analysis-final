# Welcome to Astronaut Analysis's documentation!

This analysis is based on publicly available astronauts data from [Wikidata](https://www.wikidata.org/wiki/Wikidata:Main_Page).
In this context, we investigated aspects such as time humans spent in space as well as the age distribution of the astronauts.

:::{only} html
<img src="../../results/humans_in_space.png" alt="Total Time Human in Space" title="Total Time Human in Space" width="125" height="100" />
<img src="../../results/female_humans_in_space.png" alt="Total Time Females in Space" title="Total Time Females in Space" width="125" height="100" />
<img src="../../results/male_humans_in_space.png" alt="Total Time Males in Space" title="Total Time Males in Space" width="125" height="100" />
<img src="../../results/boxplot.png" alt="Age Distribution Box Plot" title="Age Distribution Box Plot" width="125" height="100" />
<img src="../../results/combined_histogram.png" alt="Age Distribution Histogram" title="Age Distribution Histogram" width="125" height="100" />
:::

## Where to go from here?

- If you want to know how to analysis the data set, please continue to the [User Guide](user).
- If you want to know how to contribute to this project, please continue the [Contributor Guide](contributor).


:::{toctree}
:maxdepth: 1
:hidden:
user
contributor
main-script
:::
